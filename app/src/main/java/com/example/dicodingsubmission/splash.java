package com.example.dicodingsubmission;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

public class splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(splash.class.getSimpleName(), "Oncreate");

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(splash.this, MainActivity.class));
                finish();

            }
        }, 3000);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(splash.class.getSimpleName(), "Onstart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(splash.class.getSimpleName(), "OnPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(splash.class.getSimpleName(), "OnResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(splash.class.getSimpleName(), "OnStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(splash.class.getSimpleName(), "OnDestroy");
    }
}
