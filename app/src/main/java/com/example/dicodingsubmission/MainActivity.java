package com.example.dicodingsubmission;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_SHORT;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvCars;
    private ArrayList<car> list = new ArrayList<>();
    private EditText editText;
    private ProgressBar progressBar;
    private carAdapter.OnItemClickCallback carListener;
    private MenuItem menuitem;


    String content = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        rvCars = findViewById(R.id.rv_category);
        rvCars.setHasFixedSize(true);
        editText = findViewById(R.id.KolomCari);
        progressBar = findViewById(R.id.progressbar);
        menuitem = findViewById(R.id.menuSearch);




        list.addAll(CarData.getListCar());


        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    performSearch();
                    return true;
                }
                return false;
            }
        });
        carListener = new carAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(car datacar) {
                showToast(datacar);
            }
        };
        showRecyclerList();
    }


    private void showRecyclerList() {
        rvCars.setLayoutManager(new LinearLayoutManager(this));
        carAdapter carAdapter = new carAdapter(list);
        carAdapter.setOnItemClickCallback(carListener);
        rvCars.setAdapter(carAdapter);


    }

    private void showToast(car datacar) {
        Toast.makeText(this, datacar.getCarName(), LENGTH_SHORT).show();

        Intent moveIntent = new Intent(MainActivity.this, ActivityDetail.class);
        moveIntent.putExtra("namaMobil", datacar.getCarName());
        startActivity(moveIntent);


    }

//    private void performSearch() {
//        content = editText.getText().toString();
////        Toast.makeText(MainActivity.this, content, Toast.LENGTH_SHORT).show();
//        editText.clearFocus();
//        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        in.hideSoftInputFromWindow(progressBar.getWindowToken(), 0);
////        loadAnswers();
//
//    }


}
