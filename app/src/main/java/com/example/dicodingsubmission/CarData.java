package com.example.dicodingsubmission;

import java.util.ArrayList;

public class CarData {

    public static String[][] datalistCar = new String[][]{
            {"$84,900", "Toyota GR Supra GT", "2019", "AT", "https://icdn2.digitaltrends.com/image/2020_supra_exterior_023_28b46ec7836aaa289b1bccd1af944a6cd456faf1.jpg", "Introducing the Toyota GR Supra A90 Edition. Finished in a dramatic new Storm Grey matt paint with 19-inch matt black alloy wheels and red leather upholstery, the A90 Edition is a celebration of Supra heritage and is limited to just 90 examples.", "541-221-2344", "2811 New Street, Eugene, Oregon", "500Nm @4500 rpm", "250 KM/h", "335HP @7000rpm"},
            {"$25,730", "Mazda MX-5 Miata Sport", "2019", "MT", "https://cdn.motor1.com/images/mgl/J0ARK/s1/2019-mazda-miata-lead.jpg", "The 2019 MX-5 Miata offers even more exhilaration with a significant upgrade in horsepower from 155 hp to 181 hp, as well as an increased redline to 7,500 rpm. Offering even more performance to enrich the drive and prime your senses.", "330-360-5036", "1190 Glenwood Avenue, Garfield Heights, Ohio", "205Nm @4000rpm", "217 KM/h", "155HP @6000rpm"},
            {"$16,000", "Honda Civic Type R EK9", "1998", "MT", "https://www.capitalseating.co.uk/content/images/thumbs/0001463_honda-civic-type-r-ek9.jpeg", "The Honda Civic Type R (Japanese: ホンダ・シビックタイプR Honda Shibikku Taipuāru) is the highest performance version of the Honda Civic manufactured by Honda Motor Company of Japan. It features a lightened and stiffened body, special engine and six speed manual only transmission, and upgraded brakes and chassis. Red is used in the Honda badge background to give it a special sporting distinction and to separate it from other models.", "334-461-0849", "1429 Lonely Oak Drive, Mobile, Alabama", "160Nm @ 7500rpm", "235 KM/h", "185HP @8200rpm"},
            {"$27,195", "Subaru WRX Sti", "2019", "MT", "https://cdn.motor1.com/images/mgl/4bv4M/s1/2019-subaru-wrx-wrx-sti-seriesgray.jpg", "Prepare yourself for the best-handling WRX and WRX STI yet. With the perfect mix of performance-engineered power and precision-tuned steering, you’ll never want to let go.", "989-325-4012", "3146 Ripple Street, Caro, Michigan ", "290Nm @4000rpm", "257 KM/h", "268HP @5600rpm"},
            {"$170,000", "Toyota Celica GT-Four RC", "1996", "MT", "https://vignette.wikia.nocookie.net/forzamotorsport/images/7/70/FH3_Toyota_Celica_92_Front.jpg/revision/latest?cb=20180309161613", "The Toyota Celica GT-Four was a high performance model of the Celica Liftback, with a turbocharged 3S-GTE engine, and full-time AWD. It was created to compete in the World Rally Championship, whose regulations dictate that a manufacturer must build road-going versions of the vehicle in sufficient numbers. These vehicles are referred to as \"homologation special vehicles\".", "773-936-7066", "587 Pringle Drive, Hickory Hills, Illinois", "302Nm @4000rpm", "245 KM/h", "239HP @6000rpm"},
            {"$172,300", "Toyota Land Cruiser", "2019", "AT", "https://cdn.gearpatrol.com/wp-content/uploads/2018/09/Toyota-Land-Cruiser-Op-Ed-gear-patrol-slide-1-1940x1300.jpg", "The 2019 Toyota Land Cruiser is an eight-passenger luxury SUV offered in one fully loaded trim level. Every Land Cruiser also comes with a 5.7-liter V8 engine that produces 381 horsepower and 544Nm of torque. It is paired to an eight-speed automatic transmission and a full-time four-wheel-drive system.", "520-345-3235", "4944 Elk Rd Little, Tucson, Arizona", "544Nm @3600rpm", "209 KM/h", "381HP @5600rpm"},
            {"$37,900", "Mini Cooper Countryman", "2019", "AT", "https://f1.media.brightcove.com/8/1078702682/1078702682_5411010301001_5410987560001-vs.jpg?pubId=1078702682&videoId=5410987560001", "The Mini Countryman is a subcompact crossover SUV, the first vehicle of this type to be launched by Mini. It was launched in 2010 and received a facelift in 2014. The second generation vehicle came out in 2017.", "270-349-0202", "3622 Straford Park, Lexington, Kentucky", "385Nm @1350rpm", "210 KM/h", "228HP @5200rpm"},
            {"$108,000", "BMW X5", "2019", "AT", "https://s.blogcdn.com/slideshows/images/slides/743/251/1/S7432511/slug/l/02-2019-bmw-x5-fd-1.jpg", "In a world where BMW’s range takes in every X-number from 1 to 7, the X5 barely raises a remark. Why wouldn’t Munich (well, Spartanburg) build a new version of its big smart crossover? Yet in its day the X5 was a revolution. Just before the last century ended, the original X5 became the first ever ‘off-roader’ that behaved like a car on the road.", "831-596-9135", "2481 Cemetery Street, Salinas, California", "447Nm @5200rpm", "250 KM/h", "335HP @5500rpm"},
            {"$150,000", "Alfa Romeo 4C", "2019", "AT", "https://www.fullertonalfaromeo.com/static/dealer-14688/spider-alfa-2019.jpg", "This Spider is a sexy, mid-engined Italian exotic carved down to affordable scale, and its cloth targa top lets the driver and one lucky passenger feel the wind in their hair. Its tiny, low-slung body is wrapped around a sport-tuned chassis that makes it feel like every commute is a track day. It's not perfect—in fact, it's far from it—but given the opportunity, it will charm you into forgiving it for all of its flaws.", "479-966-6590", "4347 Green Hill Road, Farmington, Arkansas", "258Nm @2200rpm", "258 KM/h", "237HP @6000rpm"},
            {"$315,800", "Aston Martin Vanquish S", "2018", "AT", "https://s.aolcdn.com/commerce/autodata/images/USC70ANC061B01303.jpg", "Beauty, power, and capable handling come together in the 2018 Aston Martin Vanquish S to make it one of the most appealing grand touring cars on the market.", "870-740-1555", "3155 Sunset Drive, Blytheville, Arkansas", "630Nm @5500rpm", "324 KM/h", "595HP @7000rpm"},
    };

    public static ArrayList<car> getListCar() {
        ArrayList<car> list = new ArrayList<>();
        for (String[] aData : datalistCar) {
            car carA = new car();
            carA.setPrice(aData[0]);
            carA.setCarName(aData[1]);
            carA.setYear(aData[2]);
            carA.setTrans(aData[3]);
            carA.setPhoto(aData[4]);
            carA.setDetail(aData[5]);
            carA.setPhone(aData[6]);
            carA.setAddress(aData[7]);
            carA.setTorque(aData[8]);
            carA.setTopspeed(aData[9]);
            carA.setHorsepower(aData[10]);

            list.add(carA);

        }
        return list;
    }
}
