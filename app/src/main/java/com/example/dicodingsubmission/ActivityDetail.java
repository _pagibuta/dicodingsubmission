package com.example.dicodingsubmission;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class ActivityDetail extends AppCompatActivity {

    private ArrayList<car> arraycar;
    private ArrayList<car>  arraycar2;
    Bundle bundle;
    String nama_mobil;
    car mobilCocok = new car();

    TextView textHarga;
    TextView textCarName;
    TextView textDetails;
    TextView textTorque;
    TextView textSpeed;
    TextView textHp;
    TextView textTrans;
    TextView textDate;
    TextView textAddress;
    TextView textPhone;
    ImageView imageCar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        textHarga = findViewById(R.id.card_price2);
        textCarName = findViewById(R.id.carname);
        textDetails = findViewById(R.id.car_detail2);
        textTorque = findViewById(R.id.car_torque);
        textSpeed = findViewById(R.id.car_speed);
        textHp = findViewById(R.id.car_hp);
        textTrans = findViewById(R.id.car_transmission);
        textDate = findViewById(R.id.car_date);
        textAddress = findViewById(R.id.car_address);
        textPhone = findViewById(R.id.car_phonenumber);
        imageCar = findViewById(R.id.card_image2);


        Button dial = (Button)findViewById(R.id.button_call);
        dial.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                String toDial = "tel: "+textPhone.getText().toString();
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(toDial)));
            }
        });

        arraycar = CarData.getListCar();
        arraycar2 = CarData.getListCar();

        bundle = getIntent().getExtras();
        nama_mobil = bundle.getString("namaMobil");

        DaftarItem();
    }


    private void DaftarItem(){
        for (int w=0; w<arraycar.size(); w++){
            car item = arraycar.get(w);
            String mobilItem = item.getCarName();
            if(mobilItem.contains(nama_mobil)){
                mobilCocok =  item;
                break;
            }
        }
        textHarga.setText(mobilCocok.getPrice());
        textCarName.setText(mobilCocok.getCarName());
        textDetails.setText(mobilCocok.getDetail());
        textTorque.setText(mobilCocok.getTorque());
        textSpeed.setText(mobilCocok.getTopspeed());
        textHp.setText(mobilCocok.getHorsepower());
        textTrans.setText(mobilCocok.getTrans());
        textDate.setText(mobilCocok.getYear());
        textAddress.setText(mobilCocok.getAddress());
        textPhone.setText(mobilCocok.getPhone());


        Glide.with(this).
                load(mobilCocok.getPhoto()).
                into(imageCar);


    }
}
