package com.example.dicodingsubmission;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class carAdapter extends RecyclerView.Adapter<carAdapter.ListViewHolder> {
    private ArrayList<car> listCar;
    private OnItemClickCallback onItemClickCallbacks;
    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallbacks = onItemClickCallback;
    }

    public carAdapter(ArrayList<car> list) {
        this.listCar = list;
    }


    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.car_data, parent, false);

        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
        car car = listCar.get(position);

        Glide.with(holder.itemView.getContext())
                .load(car.getPhoto())
                .into(holder.imgPhoto);

        holder.tvPrice.setText(car.getPrice());
        holder.tvName.setText(car.getCarName());
        holder.tvYear.setText(car.getYear());
        holder.tvTrans.setText(car.getTrans());





        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickCallbacks.onItemClicked(listCar.get(holder.getAdapterPosition()));
            }
        });


    }

    @Override
    public int getItemCount() {

        return listCar.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvPrice, tvName, tvYear, tvTrans;
        Button buttonIntent;
        RecyclerView rvcar;


        ListViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.card_image);
            tvPrice = itemView.findViewById(R.id.card_price);
            tvName = itemView.findViewById(R.id.card_brand);
            tvYear = itemView.findViewById(R.id.card_year);
            tvTrans = itemView.findViewById(R.id.card_transmission);
//            buttonIntent = itemView.findViewById(R.id.button_car);

        }
    }

    public interface OnItemClickCallback {
        void onItemClicked(car datacar );
    }
}
